from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTPage, LTTextBoxHorizontal
from pdfminer.pdfdevice import PDFDevice
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFPageInterpreter, PDFResourceManager
from pdfminer.pdfpage import PDFPage, PDFTextExtractionNotAllowed
from pdfminer.pdfparser import PDFParser

file_path = './data/pdf-test.pdf'

# Open a PDF file.
fp = open(file_path, 'rb')

# Create a PDF parser object associated with the file object.
parser = PDFParser(fp)

# Create a PDF document object that stores the document structure.
# Supply the password for initialization.
document = PDFDocument(parser)

# Get the outlines of the document.
outlines = document.get_outlines()
# if outlines:
    # for (level,title,dest,a,se) in outlines:
        # print (level, title)

# Check if the document allows text extraction. If not, abort.
if not document.is_extractable:
    raise PDFTextExtractionNotAllowed

# Create a PDF resource manager object that stores shared resources.
resourceManager = PDFResourceManager()

# Create a PDF device object.
laparams = LAParams()
device = PDFPageAggregator(resourceManager, laparams=laparams)

# Create a PDF interpreter object.
interpreter = PDFPageInterpreter(resourceManager, device)


print(document.is_extractable)

LTPage()

# Process each page contained in the document.
for page in PDFPage.create_pages(document):
    interpreter.process_page(page)
    layout = device.get_result()
    print(layout)

