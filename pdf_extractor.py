from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
import json
import textract

class PdfExtractor():
    def __init__(self, file_path):
        self.file_path = file_path
        self.fp = open(self.file_path, 'rb')
        self.response = {}
        
        parser = PDFParser(self.fp)
        document = PDFDocument(parser)
        
        text = textract.process(self.file_path)
        # self.response['raw'] = text.decode()
        
        outlines = document.get_outlines()

        if outlines:
            table_of_contents = []
            for (level, title, dest, a, se) in outlines:
                table_of_contents.append({
                    'level': level,
                    'title': title,
                    'dest': dest,
                    # 'a': a.resolve(),
                    'se': se
                })
            self.response['table_of_contents'] = table_of_contents
    
    def to_json(self):
        return json.dumps(self.response)